/** SCRIPTS **/

use lucenelanches;

INSERT INTO bread (name_pt, name_en, value) VALUES ('Pão branco', 'White bread', 2.50), ('Pão australiano', 'Australian bread', 4.00), 
('Pão integral', 'Whole grain bread', 4.50), ('Pão sírio', 'Syrian bread', 5.00);

INSERT INTO cheese (name_pt, name_en, value) VALUES ('Queijo Gorgonzola', 'Gorgonzola', 3.30), ('Queijo Mussarela', 'Mozzarella cheese', 3.75), 
('Queijo Parmesão', 'Parmesan cheese', 4.10), ('Queijo Provolone', 'Provolone cheese', 5.15);

INSERT INTO filling (name_pt, name_en, value) VALUES ('Frango', 'Chicken', 5.60), ('Hamburger com bacon', 'Hamburger with bacon', 7.00), ('Hamburger', 'Hamburger', 6.50), ('Atum', 'Tuna fish', 8.00);

INSERT INTO salad (name_pt, name_en, value) VALUES ('Alface', 'Lettuce', 1.50), ('Rúcula', 'Arugula', 1.65), ('Acelga', 'Chard', 1.70);

INSERT INTO snack (bread_id, cheese_id, filling_id, salad_id, value, is_default) VALUES (1, 2, 2, 1, 14.75, 1), (2, 1, 4, 3, 17.00, 1), (1, 3, 3, 2, 14.75, 1);

INSERT INTO snack_default (name, snack_id) VALUES ('Lanche tradicional com bacon', 1), ('Lanche natural', 2), ('Lanche tradicional', 3);

INSERT INTO spice (name_pt, name_en, value) VALUES ('Pimenta', 'Chili', 1.00), ('Sal', 'Salt', 0.50), ('Orégano', 'Oregano', 0.50);

INSERT INTO sauce (name_pt, name_en, value) VALUES ('Italiano', 'Italian', 2.50), ('Apimentado', 'Spicy', 1.50);
