package br.com.entelgy.lucenelanches.vo;

import br.com.entelgy.lucenelanches.domain.Bread;
import br.com.entelgy.lucenelanches.domain.Cheese;
import br.com.entelgy.lucenelanches.domain.Filling;
import br.com.entelgy.lucenelanches.domain.Salad;
import br.com.entelgy.lucenelanches.domain.Sauce;
import br.com.entelgy.lucenelanches.domain.SnackDefault;
import br.com.entelgy.lucenelanches.domain.Spice;

public class InitialVO {
	
	private Iterable<SnackDefault> snacksDefault;
	
	private Iterable<Bread> breads;
	
	private Iterable<Cheese> cheeses;
	
	private Iterable<Filling> fillings;
	
	private Iterable<Salad> salads;
	
	private Iterable<Sauce> sauces;
	
	private Iterable<Spice> spices;
	
	public InitialVO() {
		
	}

	public Iterable<SnackDefault> getSnacksDefault() {
		return snacksDefault;
	}

	public void setSnacksDefault(Iterable<SnackDefault> snacksDefault) {
		this.snacksDefault = snacksDefault;
	}

	public Iterable<Bread> getBreads() {
		return breads;
	}

	public void setBreads(Iterable<Bread> breads) {
		this.breads = breads;
	}

	public Iterable<Cheese> getCheeses() {
		return cheeses;
	}

	public void setCheeses(Iterable<Cheese> cheeses) {
		this.cheeses = cheeses;
	}

	public Iterable<Filling> getFillings() {
		return fillings;
	}

	public void setFillings(Iterable<Filling> fillings) {
		this.fillings = fillings;
	}

	public Iterable<Salad> getSalads() {
		return salads;
	}

	public void setSalads(Iterable<Salad> salads) {
		this.salads = salads;
	}

	public Iterable<Sauce> getSauces() {
		return sauces;
	}

	public void setSauces(Iterable<Sauce> sauces) {
		this.sauces = sauces;
	}

	public Iterable<Spice> getSpices() {
		return spices;
	}

	public void setSpices(Iterable<Spice> spices) {
		this.spices = spices;
	}
	
}
