package br.com.entelgy.lucenelanches.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import br.com.entelgy.lucenelanches.domain.emums.PaymentMethodEmum;

@Entity
public class Payment {

	@Id
	@GeneratedValue
	private Long id;

	@Enumerated
	private PaymentMethodEmum method;
	
	@Column(name = "need_change")
	private Boolean change;

	@Column(length = 99)
	private Integer persons;
	
	private Float valuePerson;

	@OneToOne
	private Order order;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "payment")
	private List<PaymentPerson> paymentPersons;

	public Payment() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PaymentMethodEmum getMethod() {
		return method;
	}

	public void setMethod(PaymentMethodEmum method) {
		this.method = method;
	}

	public Boolean getChange() {
		return change;
	}

	public void setChange(Boolean change) {
		this.change = change;
	}

	public Integer getPersons() {
		return persons;
	}

	public void setPersons(Integer persons) {
		this.persons = persons;
	}

	public Float getValuePerson() {
		return valuePerson;
	}

	public void setValuePerson(Float valuePerson) {
		this.valuePerson = valuePerson;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public List<PaymentPerson> getPaymentPersons() {
		return paymentPersons;
	}

	public void setPaymentPersons(List<PaymentPerson> paymentPersons) {
		this.paymentPersons = paymentPersons;
	}

}

