package br.com.entelgy.lucenelanches.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.entelgy.lucenelanches.domain.Payment;
import br.com.entelgy.lucenelanches.domain.repository.PaymentRepository;

@Service
@Transactional
public class PaymentService {

	@Autowired
	private PaymentRepository repository;

	public PaymentService() {

	}

	public void post(Payment payment) {
		repository.save(payment);
	}
}
