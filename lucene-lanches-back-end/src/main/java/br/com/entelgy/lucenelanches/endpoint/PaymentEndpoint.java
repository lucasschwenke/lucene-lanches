package br.com.entelgy.lucenelanches.endpoint;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.entelgy.lucenelanches.domain.Payment;
import br.com.entelgy.lucenelanches.service.PaymentService;

@RestController
@RequestMapping(value = "/payment")
public class PaymentEndpoint {

	private PaymentService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Boolean> post(@RequestBody Payment payment) {
		service.post(payment);
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}

}
