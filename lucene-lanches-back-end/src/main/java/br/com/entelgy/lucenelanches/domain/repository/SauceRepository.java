package br.com.entelgy.lucenelanches.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.entelgy.lucenelanches.domain.Sauce;

@RepositoryRestResource
public interface SauceRepository extends CrudRepository<Sauce, Long> {

}
