/**
 * Este endpoint retorna todas as informações que a view precisa para ser carregada.
 */

package br.com.entelgy.lucenelanches.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.entelgy.lucenelanches.service.InitialService;
import br.com.entelgy.lucenelanches.vo.InitialVO;

@RestController
@RequestMapping(value = "/initial")
public class InitialEndpoint {

	@Autowired
	private InitialService service;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<InitialVO> get() {
		return new ResponseEntity<InitialVO>(service.get(), HttpStatus.OK);
	}

}
