package br.com.entelgy.lucenelanches.vo;

import java.util.List;

import br.com.entelgy.lucenelanches.domain.Order;
import br.com.entelgy.lucenelanches.domain.Snack;

public class OrderSnackVO {

	private Order order;
	private List<Snack> snacks;

	public OrderSnackVO() {

	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public List<Snack> getSnacks() {
		return snacks;
	}

	public void setSnacks(List<Snack> snacks) {
		this.snacks = snacks;
	}

}
