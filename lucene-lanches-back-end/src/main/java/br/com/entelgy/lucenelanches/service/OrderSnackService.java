package br.com.entelgy.lucenelanches.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.entelgy.lucenelanches.domain.Order;
import br.com.entelgy.lucenelanches.domain.OrderSnack;
import br.com.entelgy.lucenelanches.domain.Snack;
import br.com.entelgy.lucenelanches.domain.repository.OrderSnackRepository;

@Service
@Transactional
public class OrderSnackService {

	@Autowired
	private OrderSnackRepository repository;

	public OrderSnackService() {

	}

	public void save(Order order, Iterable<Snack> snacks) {

		List<OrderSnack> persist = new ArrayList<>();

		for (Snack snack : snacks) {
			OrderSnack orderSnack = new OrderSnack();
			orderSnack.setDate(new Date());
			orderSnack.setOrder(order);
			orderSnack.setSnack(snack);

			persist.add(orderSnack);
		}

		repository.save(persist);
	}

}
