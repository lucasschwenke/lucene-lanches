/**
 * Classe Bread que é um tipo de Ingredient.
 */
package br.com.entelgy.lucenelanches.domain;

import javax.persistence.Entity;

import br.com.entelgy.lucenelanches.domain.abstracts.Ingredient;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@Entity
public class Bread extends Ingredient {

	public Bread() {
		
	}
	
}
