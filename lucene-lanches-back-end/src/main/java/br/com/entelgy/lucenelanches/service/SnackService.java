/**
 * Serviço relacionado a classe Snack.
 */
package br.com.entelgy.lucenelanches.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.entelgy.lucenelanches.domain.Snack;
import br.com.entelgy.lucenelanches.domain.repository.SnackRepository;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@Service
@Transactional
public class SnackService {

	@Autowired
	private SnackRepository repository;
	
	public SnackService() {
		
	}
	
	public void save(Iterable<Snack> snacks) {
		
		List<Snack> persist = new ArrayList<>();
		
		// Aqui estou retirando os snacks padrões para adicionar apenas os customizados pelo usuario.
		for (Snack snack : snacks) {
			if (!snack.getIsDefault()) {
				persist.add(snack);
			}
		}
		
		if (!persist.isEmpty()) {
			repository.save(persist);
		}
	}
}
