/**
 * 
 */
package br.com.entelgy.lucenelanches.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.entelgy.lucenelanches.domain.Order;
import br.com.entelgy.lucenelanches.domain.Snack;
import br.com.entelgy.lucenelanches.service.OrderService;
import br.com.entelgy.lucenelanches.service.SnackService;
import br.com.entelgy.lucenelanches.vo.OrderSnackVO;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@RestController
@RequestMapping(value = "/order")
public class OrderEndpoint {

	@Autowired
	private OrderService orderService;

	@Autowired
	private SnackService snackService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Order> newOrder(@RequestBody OrderSnackVO orderSnackVO) {
		Order order = orderSnackVO.getOrder();
		Iterable<Snack> snacks = orderSnackVO.getSnacks();

		snackService.save(snacks);
		orderService.save(order, snacks);

		return new ResponseEntity<Order>(order, HttpStatus.CREATED);
	}
}
