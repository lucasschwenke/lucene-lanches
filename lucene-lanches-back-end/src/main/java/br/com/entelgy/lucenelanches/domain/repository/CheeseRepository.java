/**
 * Repository da classe Cheese.
 */
package br.com.entelgy.lucenelanches.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.entelgy.lucenelanches.domain.Cheese;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@RepositoryRestResource
public interface CheeseRepository extends CrudRepository<Cheese, Long>{

}
