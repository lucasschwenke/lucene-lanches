/**
 * Repository da classe Order.
 */
package br.com.entelgy.lucenelanches.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.entelgy.lucenelanches.domain.Order;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@RepositoryRestResource
public interface OrderRepository extends CrudRepository<Order, Long>{

}
