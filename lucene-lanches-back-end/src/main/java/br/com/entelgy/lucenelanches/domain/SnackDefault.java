package br.com.entelgy.lucenelanches.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class SnackDefault {

	@Id
	@GeneratedValue
	private Integer id;
	
	private String name;
	
	@OneToOne(fetch = FetchType.EAGER)
	private Snack snack;
	
	public SnackDefault() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Snack getSnack() {
		return snack;
	}

	public void setSnack(Snack snack) {
		this.snack = snack;
	}
	
	
}
