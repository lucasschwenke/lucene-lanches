package br.com.entelgy.lucenelanches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuceneLanchesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuceneLanchesApplication.class, args);
	}
}
