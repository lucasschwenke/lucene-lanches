/**
 * Classe Salad
 */
package br.com.entelgy.lucenelanches.domain;

import javax.persistence.Entity;

import br.com.entelgy.lucenelanches.domain.abstracts.Ingredient;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@Entity
public class Salad extends Ingredient {

	public Salad() {

	}

}
