package br.com.entelgy.lucenelanches.domain.emums;

public enum PaymentMethodEmum {

	CREDIT, DEBIT, MONEY;

}
