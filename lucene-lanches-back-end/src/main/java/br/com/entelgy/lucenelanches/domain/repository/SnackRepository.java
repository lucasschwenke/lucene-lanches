/**
 * Repository da classe Snack.
 */
package br.com.entelgy.lucenelanches.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.entelgy.lucenelanches.domain.Snack;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@RepositoryRestResource
public interface SnackRepository extends CrudRepository<Snack, Long>{

}
