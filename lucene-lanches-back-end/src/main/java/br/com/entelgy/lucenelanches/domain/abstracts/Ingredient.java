/**
 * Classe abstrata Ingredient.
 */
package br.com.entelgy.lucenelanches.domain.abstracts;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * 
 * @author Lucas Schwenke Paixão
 *
 */
@MappedSuperclass
public abstract class Ingredient {

	@Id
	@GeneratedValue
	protected Long id;

	protected String namePt;
	
	protected String nameEn;

	protected Float value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePt() {
		return namePt;
	}

	public void setNamePt(String namePt) {
		this.namePt = namePt;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

}
