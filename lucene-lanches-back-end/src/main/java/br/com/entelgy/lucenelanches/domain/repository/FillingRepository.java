/**
 * Repository da classe Filling.
 */
package br.com.entelgy.lucenelanches.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.entelgy.lucenelanches.domain.Filling;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@RepositoryRestResource
public interface FillingRepository extends CrudRepository<Filling, Long> {

}
