/**
 * Repository da classe Bread.
 */
package br.com.entelgy.lucenelanches.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.entelgy.lucenelanches.domain.Bread;

/**
 * @author Lucas Schwenke Paixão
 *
 */

@RepositoryRestResource
public interface BreadRepository extends CrudRepository<Bread, Long> {

}
