package br.com.entelgy.lucenelanches.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class SnackSpice {
	
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	private Snack snack;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Spice spice;
	
	public SnackSpice() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Snack getSnack() {
		return snack;
	}

	public void setSnack(Snack snack) {
		this.snack = snack;
	}

	public Spice getSpice() {
		return spice;
	}

	public void setSpice(Spice spice) {
		this.spice = spice;
	}
	
}
