package br.com.entelgy.lucenelanches.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.entelgy.lucenelanches.domain.repository.BreadRepository;
import br.com.entelgy.lucenelanches.domain.repository.CheeseRepository;
import br.com.entelgy.lucenelanches.domain.repository.FillingRepository;
import br.com.entelgy.lucenelanches.domain.repository.SaladRepository;
import br.com.entelgy.lucenelanches.domain.repository.SauceRepository;
import br.com.entelgy.lucenelanches.domain.repository.SnackDefaultRepository;
import br.com.entelgy.lucenelanches.domain.repository.SpiceRepository;
import br.com.entelgy.lucenelanches.vo.InitialVO;

@Service
@Transactional
public class InitialService {
	
	@Autowired
	private SnackDefaultRepository snackDefaultRepository;
	
	@Autowired
	private BreadRepository breadRepository;
	
	@Autowired
	private CheeseRepository cheeseRepository;
	
	@Autowired
	private FillingRepository fillingRepository;
	
	@Autowired
	private SaladRepository saladRepository;
	
	@Autowired
	private SauceRepository sauceRepository;
	
	@Autowired
	private SpiceRepository spiceRepository;
	
	public InitialService() {

	}
	
	public InitialVO get() {
		InitialVO vo = new InitialVO();
		
		vo.setSnacksDefault(snackDefaultRepository.findAll());
		vo.setBreads(breadRepository.findAll());
		vo.setCheeses(cheeseRepository.findAll());
		vo.setFillings(fillingRepository.findAll());
		vo.setSalads(saladRepository.findAll());
		vo.setSauces(sauceRepository.findAll());
		vo.setSpices(spiceRepository.findAll());
		
		return vo;
	}
}
