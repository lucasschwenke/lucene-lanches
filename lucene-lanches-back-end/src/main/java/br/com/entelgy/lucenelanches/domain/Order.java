/**
 * Classe Order.
 */
package br.com.entelgy.lucenelanches.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Lucas Schwenke Paixão.
 *
 */

@Entity
@Table(name = "Orders") //adicionei essa anotação pois 'order' é uma palavra reservada do MYSQL e quando tenta dar um create table order ocorre erro.
@JsonIgnoreProperties({"orderSnacks"})
public class Order {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(length = 75)
	private String name;
	
	@Column(length = 100)
	private String address;

	private Float value;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "order")
	private List<OrderSnack> orderSnacks;

	public Order() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public List<OrderSnack> getOrderSnacks() {
		return orderSnacks;
	}

	public void setOrderSnacks(List<OrderSnack> orderSnacks) {
		this.orderSnacks = orderSnacks;
	}

}
