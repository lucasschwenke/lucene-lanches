/**
 * Classe Filling que é um tipo de Ingredient.
 */
package br.com.entelgy.lucenelanches.domain;

import javax.persistence.Entity;

import br.com.entelgy.lucenelanches.domain.abstracts.Ingredient;

/**
 * @author Lucas
 *
 */

@Entity
public class Filling extends Ingredient {

	public Filling() {
	
	}
	
}
