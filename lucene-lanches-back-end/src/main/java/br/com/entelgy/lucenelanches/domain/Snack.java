/**
 * Classe Snack.
 */
package br.com.entelgy.lucenelanches.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Lucas Schwenke Paixão.
 *
 */

@Entity
@JsonIgnoreProperties({ "snackSauces", "snackSpices" })
public class Snack {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	private Bread bread;

	@ManyToOne
	private Cheese cheese;

	@ManyToOne
	private Filling filling;

	@ManyToOne
	private Salad salad;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "snack")
	private List<SnackSauce> snackSauces;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "snack")
	private List<SnackSpice> snackSpices;

	private Float value;

	private Boolean isDefault;

	public Snack() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bread getBread() {
		return bread;
	}

	public void setBread(Bread bread) {
		this.bread = bread;
	}

	public Cheese getCheese() {
		return cheese;
	}

	public void setCheese(Cheese cheese) {
		this.cheese = cheese;
	}

	public Filling getFilling() {
		return filling;
	}

	public void setFilling(Filling filling) {
		this.filling = filling;
	}

	public Salad getSalad() {
		return salad;
	}

	public void setSalad(Salad salad) {
		this.salad = salad;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public List<SnackSauce> getSnackSauces() {
		return snackSauces;
	}

	public void setSnackSauces(List<SnackSauce> snackSauces) {
		this.snackSauces = snackSauces;
	}

	public List<SnackSpice> getSnackSpices() {
		return snackSpices;
	}

	public void setSnackSpices(List<SnackSpice> snackSpices) {
		this.snackSpices = snackSpices;
	}

}
