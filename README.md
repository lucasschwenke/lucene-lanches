# Lucene Lanches

Projeto configurado no java 8

Banco de dados utilizado, MYSQL

Baixar [NodeJS](https://nodejs.org/en/)

Criar banco de dados com o seguinte nome: lucenelanches

Dentro de 'lucene-lanches-back-end/src/main/resources/application.properties' alterar para a configuração do mysql local

Executar o seguinte comando na pasta lucene-lanches-back-end: 'mvn spring-boot:run'

Em seguida executar o script dentro de 'lucene-lanches-back-end/src/main/resources/script.sql'

Executar o comando 'npm install' dentro de 'lucene-lanches-front-end'

Em seguida executar comando 'npm start'

Acesse http://localhost:8080
