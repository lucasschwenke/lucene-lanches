'use strict';

angular.module('luceneLanchesApp.payment', [])

.controller('PaymentCtrl', function($scope, orderFactory, paymentService, $translate) {

  $scope.init = function () {
    $scope.payment = {};
    $scope.payment.persons = 1;
    $scope.payment.paymentPersons = [];

    $scope.payment.valuePerson = 0;
    $scope.payment.order = orderFactory.getCompleteOrder();
  };

  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);
    $scope.lang = langKey;
  };

  $scope.addNewPerson = function () {
    $scope.payment.paymentPersons = [];

    if ($scope.payment.persons > 1) {
      $scope.payment.valuePerson = ($scope.payment.order.value / $scope.payment.persons); //calcula o novo valor divido pela quantidade de pessoas.

      for (var i = 1; i <= $scope.payment.persons; i++) {
        $scope.payment.paymentPersons.push( {'person': i, 'value': 0, 'percentage': 0} );
      }

    } else {
      var order = orderFactory.getCompleteOrder();

      $scope.payment.valuePerson = order.value;
    }

  }

  $scope.calcValue = function (person) {
    person.value = ($scope.payment.order.value * person.percentage) / 100;

    return person;
  };

  $scope.calcPercentage = function (person) {
    person.percentage = (person.value / $scope.payment.order.value) * 100;

    return person;
  };

  $scope.teste = function () {
    console.log($scope.payment);
  }

  $scope.init();

});
