angular.module('luceneLanchesApp').service('orderService', function ($http, config) {
  this.post = function (order) {
    return $http.post(config.baseUrl + '/order', order);
  };
});
