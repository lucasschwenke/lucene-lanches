angular.module('luceneLanchesApp').service('initialService', function ($http, config) {
  this.get = function () {
    return $http.get(config.baseUrl + '/initial');
  };
});
