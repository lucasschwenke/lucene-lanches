angular.module('luceneLanchesApp').service('paymentService', function ($http, config) {
  this.post = function (payment) {
    return $http.post(config.baseUrl + '/payment', payment);
  };
});
