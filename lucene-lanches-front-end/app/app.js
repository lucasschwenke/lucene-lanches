'use strict';

angular.module('luceneLanchesApp', [
  'ngRoute',
  'ui.bootstrap',
  'ui-notification',
  'ui.utils.masks',
  'pascalprecht.translate',
  'luceneLanchesApp.order',
  'luceneLanchesApp.payment'
])
.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
    .when('/order', {
      templateUrl: 'order/order.html',
      controller: 'OrderCtrl'
    })

    .when('/payment', {
      templateUrl: 'payment/payment.html',
      controller: 'PaymentCtrl'
    })

    .otherwise({redirectTo: '/order'});
}]);
