'use strict';

angular.module('luceneLanchesApp.order', [])

.controller('OrderCtrl', function($scope, $location, initialService, orderService, orderFactory, Notification, $translate) {
    $scope.order = {};
    $scope.order.value = 0.00;
    $scope.snacks = [];

    $scope.init = function() {
        $scope.snack = {};

        $scope.lang = 'pt';

        $scope.resetValue();
        $scope.getValues();
    };

    $scope.changeLanguage = function (langKey) {
      $translate.use(langKey);
      $scope.lang = langKey;
    };

    $scope.getValues = function() {
        $scope.listOfSnacksDefaults = [];
        $scope.listOfBreads = [];
        $scope.listOfCheeses = [];
        $scope.listOfFillings = [];
        $scope.listOfSalads = [];
        $scope.listOfSauces = [];
        $scope.listOfSpices = [];

        initialService.get()
            .then(function(response) {
                $scope.listOfSnacksDefaults = response.data.snacksDefault;
                $scope.listOfBreads = response.data.breads;
                $scope.listOfCheeses = response.data.cheeses;
                $scope.listOfFillings = response.data.fillings;
                $scope.listOfSalads = response.data.salads;
                $scope.listOfSauces = response.data.sauces;
                $scope.listOfSpices = response.data.spices;

            }, function(error) {
                console.log(data);

            });
    };

    $scope.resetValue = function() {
        $scope.snack.value = 0.00;
    }

    $scope.clearSnackDefault = function() {
        if (angular.isDefined($scope.default)) {
            $scope.default = {};
        }

        $scope.snack.isDefault = false;

        $scope.resetValue();
        $scope.calcValue();
    };

    $scope.calcValue = function() {
        $scope.snack.value += angular.isDefined($scope.snack.bread) ? $scope.snack.bread.value : 0.00;
        $scope.snack.value += angular.isDefined($scope.snack.cheese) ? $scope.snack.cheese.value : 0.00;
        $scope.snack.value += angular.isDefined($scope.snack.filling) ? $scope.snack.filling.value : 0.00;
        $scope.snack.value += angular.isDefined($scope.snack.salad) ? $scope.snack.salad.value : 0.00;
    };

    $scope.doubleCheese = function() {
        if (angular.isDefined($scope.snack.cheese)) {
            if ($scope.dCheese) {
                $scope.snack.value += $scope.snack.cheese.value;
            } else {
                $scope.snack.value -= $scope.snack.cheese.value;
            }
        }
    };

    $scope.doubleFilling = function() {
        if (angular.isDefined($scope.snack.filling)) {
            if ($scope.dFilling) {
                $scope.snack.value += $scope.snack.filling.value;
            } else {
                $scope.snack.value -= $scope.snack.filling.value;
            }
        }
    };

    $scope.doubleSalad = function() {
        if (angular.isDefined($scope.snack.salad)) {
            if ($scope.dSalad) {
                $scope.snack.value += $scope.snack.salad.value;
            } else {
                $scope.snack.value -= $scope.snack.salad.value;
            }
        }
    };

    $scope.setSnackDefault = function() {
        if (angular.isDefined($scope.default)) {
            $scope.snack.bread = $scope.default.snack.bread;
            $scope.snack.cheese = $scope.default.snack.cheese;
            $scope.snack.filling = $scope.default.snack.filling;
            $scope.snack.salad = $scope.default.snack.salad;
            $scope.snack.isDefault = true;

            $scope.snack.value = ($scope.default.snack.bread.value +
                $scope.default.snack.cheese.value +
                $scope.default.snack.filling.value +
                $scope.default.snack.salad.value);
        }
    };

    $scope.addSnack = function() {
        var newSnack = angular.copy($scope.snack);
        $scope.snacks.push(newSnack);

        $scope.order.value += newSnack.value;
    };

    $scope.saveNewOrder = function() {
        if (!$scope.snacks.length <= 0) {
            $scope.vo = {};
            $scope.vo.order = $scope.order;
            $scope.vo.snacks = $scope.snacks;

            orderService.post($scope.vo)
                .then(function(response) {
                    orderFactory.setCompleteOrder(response.data);
                    $location.url("/payment");

                }, function(error) {
                    console.log(error);

                });
        } else {
            if ($scope.lang === 'pt') {
                Notification.error('Ao menos um lanche deve ser selecionado para o pedido.');
            } else {
                Notification.error('At least one snack must be selectedfor the order.');
            }
        }
    };

    $scope.init();
});
