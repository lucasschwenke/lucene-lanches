angular.module("luceneLanchesApp").factory("orderFactory", function () {

  var order = {};

  var _setCompleteOrder = function (o) {
    order = o;
  };

  var _getCompleteOrder = function () {
    return order;
  };

  return {
    getCompleteOrder: _getCompleteOrder,
    setCompleteOrder: _setCompleteOrder
  };

});
